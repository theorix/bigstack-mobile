import 'package:bigstack_mobile/networking/CustomException.dart';
import 'package:dio/dio.dart';

import 'dart:io';
import 'dart:convert';
import 'dart:async';
// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:bigstack_mobile/helpers.dart';
import 'package:bigstack_mobile/services/storage.dart';

handleRequestErrors(dynamic e) {
  // The request was made and the server responded with a status code
  // that falls out of the range of 2xx and is also not 304.
  if (e.response == null) {
    print(e.response.data);
    print(e.response.headers);
    print(e.response.request);
  } else {
    // Something happened in setting up or sending the request that triggered an Error
    //print(e.request);
    throw BadRequestException('Error: could not post to: ${e.request}');
    // print(e.message);
  }
}

class ApiProvider {
  final String _baseUrl = "https://isure.ng:3000";
  // final String _baseUrl = "http://isure.ng:3000";
  //final storage = FlutterSecureStorage();
  Dio dio = new Dio();
  final StorageService storageService = StorageService();
  final Logger logger = Logger(context: 'API-Provider');

  Future<dynamic> get(String url) async {
    print('PARAM URL: ' + url);
    var responseJson;
    try {
      var response;
      var token =
          await storageService.getStoredData('jwt'); //storage.read(key: 'jwt');
      url = url[0] == '/' ? url : '/$url';
      if (token != null) {
        response = await dio.get(_baseUrl + url,
            options: Options(headers: {'token': token}));
      } else
        response = await dio.get(_baseUrl + url);
      print(_baseUrl + url);
      responseJson = _response(response);
    } on DioError catch (e) {
      // throw FetchDataException('No Internet connection');
      handleRequestErrors(e);
    }
    return responseJson;
  }

  Future<dynamic> post(String url, dynamic data) async {
    var responseJson;
    var response;
    try {
      var token =
          await storageService.getStoredData('jwt'); //storage.read(key: 'jwt');
      url = url[0] == '/' ? url : '/$url';
      if (token != null) {
        logger.log('TOKEN: ' + token);
        response = await dio.post(_baseUrl + url,
            data: data, options: Options(headers: {'token': token}));
      } else {
        logger.log('posting without jwt to: ${_baseUrl + url}');
        response = await dio.post(_baseUrl + url, data: data);
        logger.log('RESPONSE: ');
      }
      responseJson = _response(response);
      return responseJson;
    } on DioError catch (e) {
      handleRequestErrors(e);
    }
  }

  dynamic _response(Response response) {
    logger.log('HTTP-RESPONSE:' + response.toString());
    switch (response.statusCode) {
      case 200:
        var responseJson;
        try {
          responseJson = response.data;
        } catch (e) {
          throw e;
        }
        return responseJson;
      case 400:
        throw BadRequestException(response.data.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.data.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  String get baseUrl => _baseUrl;
}
