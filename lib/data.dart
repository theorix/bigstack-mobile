import 'package:bigstack_mobile/enum.dart';
import 'package:bigstack_mobile/models/menu_info.dart';
import 'package:flutter/material.dart';

List<MenuInfo> menuItems = [
  MenuInfo(MenuType.home, title: 'Home', route: '/dashboard', icon: Icons.chat),
  MenuInfo(MenuType.home,
      title: 'Finance', route: '/finance', icon: Icons.home),
  MenuInfo(MenuType.home,
      title: 'Human Resource', route: '/hr', icon: Icons.chat),
  MenuInfo(MenuType.home, title: 'Tech Hub', route: '/hub', icon: Icons.chat),
];
