import 'dart:async';
import 'dart:convert';
// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:bigstack_mobile/models/User.dart';
import 'package:bigstack_mobile/models/Hub.dart';
import 'package:bigstack_mobile/networking/ApiProvider.dart';
import 'package:bigstack_mobile/helpers.dart';
import 'package:bigstack_mobile/services/location.dart';
import 'package:bigstack_mobile/services/storage.dart';

class UserRepository {
  ApiProvider apiProvider = ApiProvider();
  final StorageService storageService = StorageService();
  Logger logger = Logger(context: 'User Repository');

  Future<User> login({String email, String password}) async {
    logger.log('Email: $email and password: $password');
    final response = await apiProvider.post(
        'user/login', json.encode({'email': email, 'password': password}));
    logger.log('USER DATA: ${response.toString()}');
    if (response != null) {
      var token = response.remove('token');
      User user = User.fromJson(response);
      storageService.storeData('jwt', token);
      if (user.isStaff) {
        final resp = await apiProvider.get('/staff/profile/${user.id}');
        // print("Profile: " + resp.toString());
        user.staffDetail = resp[0];
      }
      storageService.storeData('user', jsonEncode(user.toJson()));
      return user;
    } else {
      print("Error logging In");
      return User();
    }
  }

  Future<User> signup({String fullName, String email, String password}) async {
    logger.log('Name: $fullName Email: $email and password: $password');
    final response = await apiProvider.post('user/signup',
        json.encode({'name': fullName, 'email': email, 'password': password}));
    logger.log('USER DATA: ${response.toString()}');
    if (response != null) {
      var token = response.remove('token');
      User user = User.fromJson(response);
      storageService.storeData('jwt', token);
      storageService.storeData('user', jsonEncode(user.toJson()));
      return user;
    } else {
      print("Error logging In");
      return User();
    }
  }

  Future<void> staffSignIn(String qrCodeResult) async {
    // make request to server
    var location = await getCurrentLocation();
    var user = await storageService.getUserData();
    apiProvider.post('/staff/sign/in', {
      'location': {
        "longitude": location.longitude,
        "latitude": location.latitude
      },
      'staffId': user['staff_detail']['staff_id'],
      'qrCodeResult': qrCodeResult
    }).then((resp) {
      print("Response from server: " + resp);
    });
  }

  Future<Map<String, dynamic>> bookHubSpace(
      Map<String, dynamic> hubModel) async {
    final response = await apiProvider.post('/hub/book', json.encode(hubModel));
    return response;
  }

  Future<dynamic> addIncome(num amount, String description) async {
    var user = await storageService.getUserData();
    return apiProvider.post('/staff/income', {
      "amount": amount,
      "description": description,
      "staffId": user['staff_detail']["staff_id"]
    });
  }
}
