import 'package:flutter/material.dart';
import 'package:bigstack_mobile/services/storage.dart';

class NavDrawer extends StatelessWidget {
  final StorageService storageService = StorageService();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
              child: Center(
                  child: Text('',
                      style: TextStyle(color: Colors.white, fontSize: 15))),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                image: DecorationImage(
                  fit: BoxFit.contain,
                  image: AssetImage('assets/images/bigstack.png'),
                ),
                // borderRadius: BorderRadius.all(const Radius.circular(200))
              )),
          _buildListTile(context, 'Welcome', Icons.input, () {
            Navigator.pushReplacementNamed(context, '/login');
          }),
          Divider(),
          _buildListTile(context, 'Terms of service', Icons.account_circle, () {
            Navigator.pushReplacementNamed(context, '/login');
          }),
          Divider(),
          _buildListTile(context, 'Admin', Icons.settings, () {
            Navigator.pushReplacementNamed(context, '/admin');
          }),
          Divider(),
          _buildListTile(context, 'Visit website', Icons.exit_to_app, () {
            Navigator.pushReplacementNamed(context, '/login');
          }),
          Divider(),
          _buildListTile(context, 'Contact', Icons.exit_to_app, () {
            Navigator.pushReplacementNamed(context, '/login');
          }),
          Divider(),
          _buildListTile(context, 'Logout', Icons.exit_to_app, () {
            print('loggin out');
            //storage.deleteAll().then((f) => Navigator.pushNamed(context, 'login'));
            storageService.removeAllStoredData();
            Navigator.pushNamed(context, '/login');
          })
        ],
      ),
    );
  }

  ListTile _buildListTile(
      context, String label, IconData icon, Function onTap) {
    return ListTile(leading: Icon(icon), title: Text(label), onTap: onTap);
  }
}
