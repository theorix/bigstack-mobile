import 'package:bigstack_mobile/models/menu_info.dart';
import 'package:flutter/material.dart';
import 'package:bigstack_mobile/helpers.dart';
import 'package:bigstack_mobile/data.dart';
import 'package:provider/provider.dart';

class Navbar extends StatefulWidget {
  final index;
  _NavbarState createState() => _NavbarState(selectedIndex: index);

  Navbar({this.index});
}

class _NavbarState extends State<Navbar> {
  int selectedIndex = 0;
  _NavbarState({this.selectedIndex});
  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 1.0,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(1.0),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey, offset: Offset(1.0, 0.0), blurRadius: 6.0)
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: menuItems
              .map((currentMenuInfo) => _buildMenuItem(currentMenuInfo))
              .toList(),
        ),
      ),
    );
  }

  SizedBox _buildRouteItem(BuildContext context, String routeName,
      IconData icon, String label, int index) {
    Color selectionColor = getColorFromHex('#24b99a');
    return SizedBox(
        child: FlatButton(
            onPressed: () {
              setState(() {
                selectedIndex = index;
              });
              Navigator.pushNamed(context, routeName);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(icon,
                    size: 17,
                    color: index == selectedIndex
                        ? selectionColor
                        : Colors.black45),
                Text(label,
                    style: TextStyle(
                        fontSize: 10,
                        color: index == selectedIndex
                            ? selectionColor
                            : Colors.black45))
              ],
            )),
        height: 50);
  }

  Widget _buildMenuItem(MenuInfo currentMenuInfo) {
    Color selectionColor = getColorFromHex('#24b99a');
    return Consumer<MenuInfo>(
      builder: (BuildContext context, MenuInfo value, Widget child) {
        var activeColor = currentMenuInfo.menuType == value.menuType
            ? selectionColor
            : Colors.black38;
        return SizedBox(
            child: FlatButton(
                onPressed: () {
                  var menuInfo = Provider.of<MenuInfo>(context, listen: false);
                  menuInfo.updateMenuInfo(currentMenuInfo);
                  Navigator.pushNamed(context, currentMenuInfo.route);
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(currentMenuInfo.icon, size: 17, color: activeColor),
                    Text(currentMenuInfo.title,
                        style: TextStyle(fontSize: 10, color: activeColor))
                  ],
                )),
            height: 50);
      },
    );
  }
}

// <Widget>[
//             _buildRouteItem(context, '/dashboard', Icons.chat, 'Home', 0),
//             _buildRouteItem(context, '/profile', Icons.home, 'Profile', 1),
//             _buildRouteItem(context, '/help', Icons.help, 'Help', 2),
//             _buildRouteItem(context, '/about', Icons.web, 'About us', 3),
//           ]
