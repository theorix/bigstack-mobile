import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:bigstack_mobile/widgets/navDrawer.dart';
// import 'package:bigstack_mobile/networking/ApiProvider.dart';
import 'package:bigstack_mobile/repository/userRepository.dart';

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  String qrCodeResult;
  bool backCamera = true;
  // var apiProvider = ApiProvider();
  var userRepository = UserRepository();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // drawer: Drawer(child: NavDrawer()),
        appBar: AppBar(
          centerTitle: false,
          title: Image.asset('assets/images/bigstack.png'),
          backgroundColor: Colors.white,
          // elevation: 0.0,
        ),
        endDrawer: Drawer(
          child: NavDrawer(),
        ),
        body: Container(
            child: Column(
          children: [
            Row(
              children: <Widget>[
                Text("Scan using:" + (backCamera ? "Front Cam" : "Back Cam")),
                IconButton(
                  icon: backCamera
                      ? Icon(Ionicons.ios_reverse_camera)
                      : Icon(Icons.camera),
                  onPressed: () {
                    setState(() {
                      backCamera = !backCamera;
                      camera = backCamera ? 1 : -1;
                    });
                  },
                )
              ],
            ),
            Center(
              child: Column(
                children: [
                  Text(
                    (qrCodeResult == null) || (qrCodeResult == "")
                        ? "Please Scan to show some result"
                        : "Result:" + qrCodeResult,
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w900),
                  ),
                  IconButton(
                    icon: Icon(MaterialCommunityIcons.qrcode_scan, size: 50),
                    onPressed: () {
                      _scan();
                    },
                  )
                ],
              ),
            )
          ],
        )));
  }

  Future<void> _scan() async {
    ScanResult codeSanner = await BarcodeScanner.scan(
      options: ScanOptions(
        useCamera: camera,
      ),
    );
    setState(() async {
      qrCodeResult = codeSanner.rawContent;
      await userRepository.staffSignIn(qrCodeResult);
    });
  }
}

int camera = 1;
