class User {
  num id;
  String name;
  String email;
  bool isStaff;
  Map<String, dynamic> staffDetail;
  num walletBalace;

  User(
      {this.id,
      this.name,
      this.email,
      this.isStaff,
      this.staffDetail,
      this.walletBalace});

  User.fromJson(Map<String, dynamic> json) {
    id = num.parse(json['id']);
    name = json['name'];
    email = json['email'];
    isStaff = json['is_staff'] == 0 ? false : true;
    staffDetail = json['staff_detail'];
    walletBalace = json['walletBalance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['is_staff'] = this.isStaff;
    data['staff_detail'] = this.staffDetail;
    data['walletBalance'] = this.walletBalace;
    return data;
  }
}
