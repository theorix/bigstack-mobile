class Plan {
  int id;
  String name;
  int price;
  String description;

  Plan({this.name, this.id, this.price, this.description});

  Map<String, dynamic> toJson() {
    return {'id': id, 'name': name, 'price': price, 'description': description};
  }
}

class Duration {
  int duration;
  String type;

  Duration({this.duration, this.type});

  Map<String, dynamic> toJson() {
    return {'duration': duration, 'type': type};
  }
}

class Booking {
  Plan hubPlan;
  Duration duration;
  DateTime startDate;
  String userEmail;

  Booking({this.userEmail, this.hubPlan, this.startDate, this.duration});

  Map<String, dynamic> toJson() {
    return {
      'hub_plan': hubPlan.toJson(),
      'duration': duration.toJson(),
      'start_date': startDate.toString(),
      'email': userEmail
    };
  }

  Booking.fromJson(Map<String, dynamic> json) {
    hubPlan = json['hub_plan'];
    duration = json['duration'];
    startDate = DateTime(json['start_date']);
    userEmail = json['email'];
  }
}
