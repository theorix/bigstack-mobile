import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import '../enum.dart';

class MenuInfo extends ChangeNotifier {
  MenuType menuType;
  String title;
  IconData icon;
  String route;

  MenuInfo(this.menuType, {this.title, this.icon, this.route});

  updateMenuInfo(MenuInfo menuInfo) {
    this.menuType = menuInfo.menuType;
    this.title = menuInfo.title;
    this.icon = menuInfo.icon;

    // very important
    notifyListeners();
  }
}
