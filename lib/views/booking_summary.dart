import 'package:flutter/material.dart';
import 'package:bigstack_mobile/models/Hub.dart';
import 'package:bigstack_mobile/networking/Response.dart';
import 'package:bigstack_mobile/blocs/UserBloc.dart';
import 'package:bigstack_mobile/helpers.dart';

class BookingSummary extends StatefulWidget {
  @override
  BookingSummaryState createState() => BookingSummaryState();
}

class BookingSummaryState extends State<BookingSummary> {
  var bookingBloc = UserBloc();

  @override
  Widget build(BuildContext context) {
    Booking bookingModel = ModalRoute.of(context).settings.arguments;
    bookingBloc.bookSpace(bookingModel.toJson());
    TextStyle textStyle = TextStyle(
        fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black);
    TextStyle textStyle2 = TextStyle(
        fontSize: 22, fontWeight: FontWeight.w400, color: Colors.deepPurple);
    return Scaffold(
        appBar: AppBar(title: Text('Booking Summary')),
        body: StreamBuilder<Response<Map<String, dynamic>>>(
            stream: bookingBloc.bookingDataStream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) return Container();
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Center(child: CircularProgressIndicator());
                  break;

                case Status.COMPLETED:
                  return Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                              child: Center(
                            child: Text('Booking Summary',
                                style: TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.w400)),
                          )),
                          Row(
                            children: [
                              Text('Package: ', style: textStyle),
                              Text(bookingModel.hubPlan.name,
                                  style: textStyle2),
                            ],
                          ),
                          Row(
                            children: [
                              Text('Price: ', style: textStyle),
                              Row(
                                children: [
                                  Text(naira(bookingModel.hubPlan.price),
                                      style: textStyle2),
                                  Text(' (' +
                                      bookingModel.hubPlan.description +
                                      ')')
                                ],
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Text('Starting on ', style: textStyle),
                              Text(
                                  myDateFormat(
                                      bookingModel.startDate.toString()),
                                  style: textStyle2)
                            ],
                          ),
                          Row(
                            children: [
                              Text('Ending on ', style: textStyle),
                              Text(
                                  myDateFormat(
                                      bookingModel.startDate.toString()),
                                  style: textStyle2)
                            ],
                          ),
                          Row(
                            children: [
                              Text('Total Amount: ', style: textStyle),
                              Text(naira(1937), style: textStyle2)
                            ],
                          ),
                          Container(
                            child: RaisedButton(
                              color: getColorFromHex('#24b99a'),
                              child: Text('Submit'),
                              onPressed: () {
                                print('Booking Completed');
                              },
                            ),
                          )
                        ],
                      ));
                  break;

                case Status.ERROR:
                  return Center(child: Text('Error booking space'));
                  break;
              }
              return Container();
            }));
  }
}
