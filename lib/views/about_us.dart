import 'package:flutter/material.dart';
import 'package:bigstack_mobile/widgets/mainNav.dart';

class AboutUsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('About Bigstack Technologies')),
        body: Center(child: Text('About us Page')),
        bottomNavigationBar: Navbar(index: 3));
  }
}
