import 'package:bigstack_mobile/networking/ApiProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:bigstack_mobile/models/Payment.dart';
import 'package:paystack_manager/paystack_pay_manager.dart';
import 'package:paystack_manager/models/transaction.dart';
import 'package:bigstack_mobile/services/storage.dart';
import 'package:bigstack_mobile/helpers.dart';
import 'dart:convert';

class PaymentScreen extends StatelessWidget {
  PaymentScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Payment Screen')), body: PaymentForm());
  }
}

class PaymentForm extends StatefulWidget {
  @override
  _PaymentFormState createState() => _PaymentFormState();
}

class _PaymentFormState extends State<PaymentForm> {
  final _key = GlobalKey<FormState>();
  CardDetails _cardDetails = CardDetails();
  String expiryMonth;
  String expiryYear;
  final List yearsList =
      List.generate(12, (int index) => index + DateTime.now().year);
  bool rememberInfo = false;

  StorageService storageService = StorageService();
  var _initialAmount = 1000;
  var _amount;
  var amountTextController;

  // String _email = 'konye@gmail.com';
  // String _email = 'johno@gmail.com';
  bool loading = false;

  var apiProvider = ApiProvider();

  var publicKey = 'pk_test_54853e4e4c031055749a1efb31f5676e233988cb';
  var secretKey = 'sk_test_48634bf2f3ae37c0323cc9b4440d1001f6f7c5d8';

  @override
  void initState() {
    super.initState();
    amountTextController =
        TextEditingController(text: _initialAmount.toString());
    _amount = _initialAmount * 100;
    amountTextController.addListener(() {
      if (amountTextController.text.isEmpty ||
          int.parse(amountTextController.text) == 0) {
        _amount = _initialAmount * 100;
      } else {
        _amount = int.parse(amountTextController.text) * 100;
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    amountTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: storageService.getUserData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var user = snapshot.data;
            // var names = user['name'].split(' ');
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: ListView(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 10),
                            child: Row(
                              children: [
                                Text('Your current balace is '),
                                Text(naira(user['walletBalance']),
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w700))
                              ],
                            )),
                        Text("Enter  an amount to fund your wallet"),
                        loading ? CircularProgressIndicator() : Container(),
                        SizedBox(height: 10),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: amountTextController,
                          decoration: InputDecoration(
                              labelText: 'Amount',
                              icon: Icon(MaterialCommunityIcons.magnet_on)),
                        ),
                        RaisedButton(
                          child: Text('Continue'),
                          color: Colors.greenAccent,
                          textColor: Colors.white,
                          onPressed: () {
                            print('Payment Complete');

                            setState(() {
                              loading = true;
                            });

                            apiProvider.post('/user/transaction/init', {
                              'email': user['email'],
                              'amount': _amount
                            }).then((respJson) {
                              print(respJson.toString());
                              PaystackPayManager(context: context)
                                ..setSecretKey(secretKey)
                                //accepts widget
                                ..setCompanyAssetImage(Image(
                                  image: AssetImage("assets/images/logo.png"),
                                ))
                                ..setAmount(_amount)
                                ..setReference(respJson['reference'] +
                                    DateTime.now()
                                        .millisecondsSinceEpoch
                                        .toString())
                                ..setCurrency("NGN")
                                ..setEmail(user['email'])
                                ..setFirstName(user['name'])
                                // ..setLastName("Bako")
                                ..setMetadata(
                                  {
                                    "custom_fields": [
                                      {
                                        "value": "snapTask",
                                        "display_name": "Payment to",
                                        "variable_name": "payment_to"
                                      }
                                    ]
                                  },
                                )
                                ..onSuccesful(_onPaymentSuccessful)
                                ..onPending(_onPaymentPending)
                                ..onFailed(_onPaymentFailed)
                                ..onCancel(_onPaymentCancelled)
                                ..initialize();
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ));
          }
          return Container();
        });
  }

  _onPaymentSuccessful(Transaction transaction) {
    storageService.getUserData().then((user) {
      apiProvider.post('/user/transaction/verify', {
        'txId': transaction.refrenceNumber,
        'email': user['email'],
        'amount': _amount,
        'description': 'Wallet funding'
      }).then((resp) {
        print(resp.toString());
        user['walletBalance'] += int.parse(amountTextController.text);
        storageService.storeData('user', jsonEncode(user));
        setState(() {
          loading = false;
        });
      });
    });
  }

  void _onPaymentPending(Transaction transaction) {
    print("Transaction is pendinng");
    print("Transaction Refrence ===> ${transaction.refrenceNumber}");
    setState(() {
      loading = false;
    });
  }

  _onPaymentFailed(error) {
    print(error.toString());
    setState(() {
      loading = false;
    });
  }

  _onPaymentCancelled(d) {
    setState(() {
      loading = false;
    });
  }

  paymentForm() {
    return Form(
        key: _key,
        child: ListView(
          children: [
            Column(
              children: [
                TextFormField(
                  onSaved: (name) => _cardDetails.cardHolderName = name,
                  decoration: InputDecoration(
                      labelText: 'Card holders name',
                      icon: Icon(MaterialCommunityIcons.account_circle)),
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  onSaved: (cardNumber) => _cardDetails.cardNumber = cardNumber,
                  decoration: InputDecoration(
                      labelText: 'Card Number',
                      icon: Icon(MaterialCommunityIcons.credit_card)),
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  onSaved: (securityCode) =>
                      _cardDetails.securityCode = securityCode,
                  decoration: InputDecoration(
                      labelText: 'Security code',
                      icon: Icon(MaterialCommunityIcons.lock_outline)),
                  maxLength: 4,
                ),
                DropdownButtonFormField<String>(
                  onSaved: (val) => _cardDetails.expiryMonth = val,
                  onChanged: (val) {
                    setState(() {
                      expiryMonth = val;
                    });
                  },
                  value: expiryMonth,
                  items: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                  ].map<DropdownMenuItem<String>>((val) {
                    return DropdownMenuItem(child: Text(val), value: val);
                  }).toList(),
                  decoration: InputDecoration(
                      labelText: 'Expiry Month',
                      icon: Icon(Icons.calendar_today)),
                ),
                DropdownButtonFormField(
                  onSaved: (val) => _cardDetails.expiryYear = val.toString(),
                  onChanged: (val) => setState(() {
                    expiryYear = val.toString();
                  }),
                  items: yearsList.map<DropdownMenuItem<String>>((val) {
                    return DropdownMenuItem(
                        value: val.toString(), child: Text(val.toString()));
                  }).toList(),
                  decoration: InputDecoration(
                      labelText: 'Expiry Year',
                      icon: Icon(Icons.calendar_today)),
                ),
                CheckboxListTile(
                  value: rememberInfo,
                  onChanged: (val) {
                    setState(() {
                      rememberInfo = val;
                    });
                  },
                ),
                RaisedButton(
                  child: Text('Process Payment'),
                  color: Colors.greenAccent,
                  textColor: Colors.white,
                  onPressed: () {
                    print('Payment Complete');
                  },
                ),
              ],
            )
          ],
        ));
  }
}
