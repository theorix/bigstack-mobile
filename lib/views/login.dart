import 'package:flutter/material.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:bigstack_mobile/views/dashboard.dart';
import 'package:bigstack_mobile/blocs/UserBloc.dart';
import 'package:bigstack_mobile/networking/Response.dart';
import 'package:bigstack_mobile/models/User.dart';
import 'package:bigstack_mobile/services/storage.dart';
import 'package:bigstack_mobile/views/signup.dart';

class UserLogin extends StatefulWidget {
  @override
  _UserLoginState createState() => _UserLoginState();
}

class _UserLoginState extends State<UserLogin> {
  TextEditingController emailEditingController = TextEditingController();
  TextEditingController passwordEditingController = TextEditingController();
  UserBloc userBloc = UserBloc();
  UserBloc userBloc2 = UserBloc();
  StorageService storageService = StorageService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: SingleChildScrollView(
                child: Column(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(top: 15),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.11,
            child: _logo(),
            color: Theme.of(context).primaryColor),
        Center(
            child: Text('Login',
                style: TextStyle(
                    letterSpacing: 1.0,
                    fontSize: 25,
                    color: Colors.green[800],
                    fontWeight: FontWeight.w700))),
        _buildFormField(
            label: 'Email',
            controller: emailEditingController,
            type: TextInputType.emailAddress),
        _buildFormField(
            label: 'Password',
            controller: passwordEditingController,
            secret: true),
        SizedBox(height: 15),
        _submitButton(),
        SizedBox(height: 15),
        Container(
            child: Row(
          children: [
            Text("Don't have an account?"),
            GestureDetector(
                child: Text('Sign up Here',
                    style: TextStyle(color: Colors.redAccent)),
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => UserSignup()));
                })
          ],
        ))
      ],
    ))));
  }

  Widget _logo() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Center(
          child: Image.asset('assets/images/bigstack.png'),
        ));
  }

  // snapshot.data['name']

  Widget _buildFormField(
      {String label,
      TextEditingController controller,
      TextInputType type = TextInputType.text,
      bool secret = false}) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 35, vertical: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(label.toUpperCase(),
                style: TextStyle(
                    letterSpacing: 1.0,
                    fontSize: 15,
                    fontWeight: FontWeight.w500)),
            TextField(
              autofocus: false,
              obscureText: secret,
              keyboardType: type,
              controller: controller,
              decoration: InputDecoration(
                  // labelText: ,
                  // hintText: label,
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                  ),
                  border: InputBorder.none),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 0.1, vertical: 0.1),
              child: Container(
                height: 2.0,
                width: MediaQuery.of(context).size.width,
                color: Colors.black12,
              ),
            )
          ],
        ));
  }

  Widget _submitButton() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        child: ButtonTheme(
          //elevation: 4,
          //color: Colors.green,
          minWidth: double.infinity,
          child: MaterialButton(
            onPressed: () {
              userBloc.userDataStream.listen((res) {
                switch (res.status) {
                  case Status.LOADING:
                    userBloc2.userDataSink.add(res);
                    break;
                  case Status.COMPLETED:
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                    userBloc2.userDataSink.add(res);
                    break;
                  default:
                    userBloc2.userDataSink.add(res);
                }
              });
              userBloc.authenticate(
                  emailEditingController.text, passwordEditingController.text);
            },
            textColor: Colors.white,
            color: Colors.green[800],
            height: 50,
            child: Text("Submit",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18)),
          ),
        ));
  }
}

class Loading extends StatelessWidget {
  final String loadingMessage;

  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor:
                AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
          ),
        ],
      ),
    );
  }
}
