import 'package:flutter/material.dart';
import 'package:bigstack_mobile/init.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  initState() {
    initApp(context: context);
    super.initState();
    // Navigator.of(context).pushNamed('/dashboard');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Center(
        child: Image.asset('assets/images/bigstack.png'),
      ),
    ));
  }
}
