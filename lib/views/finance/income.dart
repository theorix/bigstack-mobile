import 'package:flutter/material.dart';
// import 'package:bigstack_mobile/networking/ApiProvider.dart';
import 'package:bigstack_mobile/repository/UserRepository.dart';

class IncomePage extends StatefulWidget {
  IncomePageState createState() => IncomePageState();
}

class IncomePageState extends State<IncomePage>
    with SingleTickerProviderStateMixin {
  String pageTitle = 'Add Income';
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      if (_tabController.index == 0) {
        setState(() {
          pageTitle = 'Add Income';
        });
      } else {
        setState(() {
          pageTitle = 'View Income';
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(pageTitle),
          bottom: TabBar(
              controller: _tabController,
              tabs: [Tab(child: Text('Add')), Tab(child: Text('View'))]),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [AddIncome(), ViewIncome()],
        ));
  }
}

class ViewIncome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text("View Income"));
  }
}

class AddIncome extends StatefulWidget {
  @override
  _AddIncomeState createState() => _AddIncomeState();
}

class _AddIncomeState extends State<AddIncome> {
  var textDescController = TextEditingController();
  var amountController = TextEditingController();
  bool networkLoading = false;

  var userRepository = UserRepository();

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Add Income",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700)),
            SizedBox(height: 10),
            networkLoading ? CircularProgressIndicator() : Container(),
            Container(
                margin: EdgeInsets.only(top: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Description"),
                    TextField(
                      controller: textDescController,
                      keyboardType: TextInputType.text,
                    ),
                    SizedBox(height: 20),
                    Text("Amount"),
                    TextField(
                      controller: amountController,
                      keyboardType: TextInputType.number,
                    ),
                    RaisedButton(
                        onPressed: () {
                          networkLoading = true;
                          userRepository
                              .addIncome(int.parse(amountController.text),
                                  textDescController.text)
                              .then((data) {
                            print("$data");
                            setState(() {
                              networkLoading = false;
                            });
                          });
                        },
                        child: Text("Submit"))
                  ],
                ))
          ],
        ));
  }
}
