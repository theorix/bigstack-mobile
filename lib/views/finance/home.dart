import 'package:bigstack_mobile/widgets/navDrawer.dart';
import 'package:bigstack_mobile/widgets/mainNav.dart';
import 'package:flutter/material.dart';
import 'package:bigstack_mobile/helpers.dart';

import 'package:bigstack_mobile/services/location.dart';
import 'package:bigstack_mobile/services/storage.dart';

class Finance extends StatelessWidget {
  bool userIsStaff = false;
  StorageService storageService = StorageService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: false,
        title: Image.asset('assets/images/bigstack.png'),
        backgroundColor: Colors.white,
        // elevation: 0.0,
      ),
      endDrawer: Drawer(
        child: NavDrawer(),
      ),
      body: Container(
          color: getColorFromHex('#EBEBEB'),
          child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Column(
                children: [
                  _buildStatus(context),
                  SingleChildScrollView(
                      child: Container(
                          height: MediaQuery.of(context).size.height * .6,
                          child: GridView.count(
                            childAspectRatio: 1.5,
                            crossAxisCount: 2,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10,
                            children: [
                              _buildItem(context,
                                  icon: Icons.military_tech,
                                  color: '#5c6bc0',
                                  title: 'Income',
                                  url: '/finance/income'),
                              _buildItem(context,
                                  icon: Icons.subscriptions_outlined,
                                  color: '#00c292',
                                  title: 'Voucher',
                                  url: '/finance/voucher'),
                              _buildItem(context,
                                  icon: Icons.school_outlined,
                                  color: '#ab8ce4',
                                  title: 'Payroll'),
                              _buildItem(context,
                                  icon: Icons.wallet_travel_outlined,
                                  color: '#5c6bc0',
                                  title: 'User Wallet',
                                  url: '/payment'),
                              _buildItem(context,
                                  icon: Icons.history,
                                  color: '#00c292',
                                  title: 'Subscriptions')
                            ],
                          )))
                ],
              ))),
      bottomNavigationBar: Navbar(),
    );
  }

  Widget _buildStatus(BuildContext context) {
    if (userIsStaff)
      return _staffStatus(context);
    else
      return _userStatus(context);
  }

  Widget _buildItem(BuildContext context,
      {IconData icon, color: String, String title, String url}) {
    return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, url);
        },
        child: Opacity(
            opacity: 1,
            child: Container(
                // margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                padding: EdgeInsets.only(left: 15, top: 15),
                decoration: BoxDecoration(
                    color: getColorFromHex(color),
                    borderRadius: BorderRadius.circular(6.0),
                    boxShadow: [
                      BoxShadow(
                          color: getColorFromHex('#EBEBEB'),
                          offset: Offset(1.0, 0.0),
                          blurRadius: 6.0)
                    ]),
                // height: 30,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(icon, size: 50, color: Colors.white),
                      SizedBox(height: 10),
                      Text(title,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w400))
                    ]))));
  }

  Widget _userStatus(context) {
    return FutureBuilder(
        future: storageService.getUserData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var user = snapshot.data;
            print("user:" + user.toString());
            return Container(
              height: MediaQuery.of(context).size.height * .15,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                  boxShadow: [BoxShadow(blurRadius: 6.0, color: Colors.grey)]),
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                children: [
                  Text('Status'),
                  Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                child: Column(children: [
                              Text('Balance', style: TextStyle(fontSize: 10)),
                              Text(naira(user['walletBalance']),
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w400,
                                      letterSpacing: 1.0))
                            ])),
                            Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Your next Hub Session',
                                        style: TextStyle(fontSize: 10)),
                                    Text(
                                        myDateFormat(DateTime.now().toString()),
                                        style: TextStyle(
                                            color: getColorFromHex('#24b99a'))),
                                    FutureBuilder(
                                        future: getCurrentLocation(),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            return Column(
                                              children: [
                                                Text('Longitude: ' +
                                                    snapshot.data.longitude
                                                        .toString()),
                                                Text('Latitude: ' +
                                                    snapshot.data.latitude
                                                        .toString()),
                                              ],
                                            );
                                          }
                                          return Container();
                                        }),
                                  ],
                                ))
                          ]))
                ],
              ),
            );
          }
          return Container();
        });
  }

  Widget _staffStatus(context) {
    return Container(
      height: MediaQuery.of(context).size.height * .15,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.white,
          boxShadow: [BoxShadow(blurRadius: 6.0, color: Colors.grey)]),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        children: [
          Text('Status'),
          Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        child: Column(children: [
                      Text('Balance', style: TextStyle(fontSize: 10)),
                      Text(naira(2345),
                          style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w400,
                              letterSpacing: 1.0))
                    ])),
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Hub Participants',
                                style: TextStyle(fontSize: 10)),
                            Text('6 of 8 active',
                                style: TextStyle(
                                    color: getColorFromHex('#24b99a')))
                          ],
                        ))
                  ]))
        ],
      ),
    );
  }
}
