import 'package:flutter/material.dart';

class VoucherPage extends StatefulWidget {
  VoucherPageState createState() => VoucherPageState();
}

class VoucherPageState extends State<VoucherPage>
    with SingleTickerProviderStateMixin {
  String pageTitle = 'Generate Voucher';
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      if (_tabController.index == 0) {
        setState(() {
          pageTitle = 'Generate Voucher';
        });
      } else {
        setState(() {
          pageTitle = 'View Voucher';
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(pageTitle),
          bottom: TabBar(
              controller: _tabController,
              tabs: [Tab(child: Text('Generate')), Tab(child: Text('View'))]),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [GenerateVoucher(), ViewVoucher()],
        ));
  }
}

class ViewVoucher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text("View Voucher"));
  }
}

class GenerateVoucher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text("Generate Voucher"));
  }
}
