import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:bigstack_mobile/services/storage.dart';
import 'package:bigstack_mobile/widgets/mainNav.dart';

class AdminView extends StatelessWidget {
  final storageService = StorageService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(),
        body: Container(
          margin: EdgeInsets.only(top: 25),
          child: FutureBuilder(
              future: storageService.getStoredData('jwt'),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  print(snapshot.data);
                  return WebView(
                      initialUrl:
                          'https://bigstacktech.com/dashboard/?token=${snapshot.data}',
                      javascriptMode: JavascriptMode.unrestricted);
                } else
                  return Container();
              }),
        ),
        bottomNavigationBar: Navbar());
  }
}

// password
// agbatom@gmail.com
