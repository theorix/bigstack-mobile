import 'package:flutter/material.dart';
import 'package:bigstack_mobile/widgets/scanQr.dart';
import 'package:bigstack_mobile/helpers.dart';
import 'package:bigstack_mobile/models/Hub.dart';
import 'package:intl/intl.dart';
import 'package:bigstack_mobile/services/storage.dart';
import 'package:bigstack_mobile/networking/ApiProvider.dart';

class HubPage extends StatefulWidget {
  HubPageState createState() => HubPageState();
}

class HubPageState extends State<HubPage> with SingleTickerProviderStateMixin {
  String pageTitle = 'Book a space';
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      if (_tabController.index == 0) {
        setState(() {
          pageTitle = 'Book a space';
        });
      } else {
        setState(() {
          pageTitle = 'Check in';
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(pageTitle),
          bottom: TabBar(controller: _tabController, tabs: [
            Tab(child: Text('Book space')),
            Tab(child: Text('Check in'))
          ]),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [BookingForm(), ScanPage()],
        ));
  }
}

class BookingForm extends StatefulWidget {
  @override
  BookingFormState createState() => BookingFormState();
}

class BookingFormState extends State<BookingForm> {
  DateTime selectedDate = DateTime.now();
  var _user;
  int duration = 1;
  int selectedDurationIndex = 0;
  String selectedPlan = "Basic 0";
  List<String> durationTypeList = ['Day', 'Week', 'Month', 'Year'];
  List<Map<String, dynamic>> _hubBasicPlans;
  List<Map<String, dynamic>> _hubExecutivePlans;
  StorageService storageService = StorageService();
  ApiProvider apiService = ApiProvider();
  Booking bookingModel;
  bool viewInited = false;

  @override
  void initState() {
    super.initState();

    // storageService.getUserData().then((user) {
    //   _user = user;
    //   bookingModel = Booking(
    //       userEmail: user['email'],
    //       hubPlan: Plan(name: 'Basic', price: 350, description: 'Workspace'),
    //       duration: Duration(duration: 1, type: 'Day'),
    //       startDate: DateTime.now());
    // });
  }

  @override
  Widget build(BuildContext context) {
    if (viewInited) {
      return Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: [
              _buildPlan('Basic', _hubBasicPlans),
              SizedBox(height: 30),
              _buildPlan('Executive', _hubExecutivePlans),
              SizedBox(height: 30),
              Row(
                children: [
                  FlatButton(
                      onPressed: () => _selectDate(context),
                      child: Column(
                        children: [
                          Text('Date'),
                          Text(DateFormat.yMMMEd().format(selectedDate))
                        ],
                      )),
                  Expanded(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        width: 30,
                        child: TextFormField(
                          onChanged: (val) {
                            if (val.isEmpty) return;
                            setState(() {
                              int d = int.parse(val);
                              bookingModel.duration.duration = d;
                              if (d > 1) {
                                durationTypeList = [
                                  'Days',
                                  'Weeks',
                                  'Months',
                                  'Years'
                                ];
                              } else {
                                durationTypeList = [
                                  'Day',
                                  'Week',
                                  'Month',
                                  'Year'
                                ];
                              }
                            });
                          },
                          keyboardType: TextInputType.number,
                          initialValue:
                              bookingModel.duration.duration.toString(),
                        ),
                      ),
                      SizedBox(width: 100, child: _generateDropdown())
                    ],
                  ))
                ],
              ),
              SizedBox(height: 30),
              Container(
                child: RaisedButton(
                    color: Colors.green,
                    onPressed: () {
                      print('Submit Form');
                      print(bookingModel.toJson().toString());
                      Navigator.pushNamed(context, '/booking/summary',
                          arguments: bookingModel);
                    },
                    child: Text('Continue',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w400))),
              )
            ],
          ));
    }
    return FutureBuilder(
        future: initView(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return CircularProgressIndicator();
        });
  }

  Widget _buildPlan(String name, List<Map<String, dynamic>> priceList) {
    List<Widget> colChildren = priceList
        .map((p) => Container(
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: GestureDetector(
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(naira(p['amount']),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400)),
                    selectedPlan == name + ' ' + priceList.indexOf(p).toString()
                        ? Icon(Icons.check, color: Colors.white, size: 35)
                        : Container(),
                    Text(p['description'], style: TextStyle(fontSize: 12))
                  ],
                ),
                onTap: () {
                  setState(() {
                    bookingModel.hubPlan.name = name;
                    bookingModel.hubPlan.price = p['amount'];
                    bookingModel.hubPlan.description = p['description'];
                    selectedPlan = name + ' ' + priceList.indexOf(p).toString();
                    print(selectedPlan);
                  });
                })))
        .toList();
    return Card(
        color: bookingModel.hubPlan.name == name
            ? Colors.greenAccent
            : Colors.white,
        child: Column(
          children: [
            Container(
                child: Text(name,
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w400,
                        color: bookingModel.hubPlan.name == name
                            ? Colors.white
                            : Colors.black))),
            Row(
              children: colChildren,
            )
          ],
        ));
  }

  Future<dynamic> initView() async {
    var user = await storageService.getUserData();
    _user = user;

    var hubData = await apiService.get('/hub/status');
    _hubBasicPlans = [
      {'amount': hubData['priceOfBasic1'], 'description': 'Workspace'},
      {'amount': hubData['priceOfBasic2'], 'description': 'Data + Workspace'}
    ];

    _hubExecutivePlans = [
      {
        'amount': hubData['priceOfExecutive1'],
        'description': 'Office workspace'
      },
      {
        'amount': hubData['priceOfExecutive2'],
        'description': 'Data + Office workspace'
      }
    ];

    bookingModel = Booking(
        userEmail: user['email'],
        hubPlan: Plan(
            name: 'Basic',
            price: _hubBasicPlans[0]['amount'],
            description: _hubBasicPlans[0]['description']),
        duration: Duration(duration: 1, type: 'Day'),
        startDate: DateTime.now());

    setState(() {
      viewInited = true;
    });

    return true;
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 12),
    );
    if (picked != null &&
        picked != selectedDate &&
        DateTime.now().compareTo(picked) < 0)
      setState(() {
        selectedDate = picked;
        bookingModel.startDate = picked;
      });
  }

  Widget _generateDropdown() {
    return DropdownButtonFormField(
        onChanged: (val) {
          setState(() {
            selectedDurationIndex = val;
            bookingModel.duration.type = durationTypeList[val];
          });
        },
        value: selectedDurationIndex,
        items: durationTypeList
            .map<DropdownMenuItem<int>>((d) => DropdownMenuItem(
                value: durationTypeList.indexOf(d), child: Text(d)))
            .toList());
  }
}
