import 'package:flutter/material.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:bigstack_mobile/views/dashboard.dart';
import 'package:bigstack_mobile/blocs/UserBloc.dart';
import 'package:bigstack_mobile/networking/Response.dart';
import 'package:bigstack_mobile/models/User.dart';
import 'package:bigstack_mobile/services/storage.dart';
import 'package:bigstack_mobile/views/login.dart';

class UserSignup extends StatefulWidget {
  @override
  _UserSignupState createState() => _UserSignupState();
}

class _UserSignupState extends State<UserSignup> {
  TextEditingController emailEditingController = TextEditingController();
  TextEditingController nameEditingController = TextEditingController();
  TextEditingController passwordEditingController = TextEditingController();
  TextEditingController passwordEditingController2 = TextEditingController();

  UserBloc userBloc = UserBloc();
  UserBloc userBloc2 = UserBloc();
  StorageService storageService = StorageService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: SingleChildScrollView(
                child: Column(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(top: 15),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.11,
            child: Center(
                child: Text('Register',
                    style: TextStyle(
                        letterSpacing: 1.0,
                        fontSize: 25,
                        color: Colors.white,
                        fontWeight: FontWeight.w700))),
            color: Theme.of(context).primaryColor),
        _logo(),
        _buildFormField(
            label: 'Full Name',
            controller: nameEditingController,
            type: TextInputType.text),
        _buildFormField(
            label: 'Email',
            controller: emailEditingController,
            type: TextInputType.emailAddress),
        _buildFormField(
            label: 'Password',
            controller: passwordEditingController,
            secret: true),
        _buildFormField(
            label: 'Confirm Password',
            controller: passwordEditingController2,
            secret: true),
        SizedBox(height: 15),
        _submitButton(),
        Container(
            child: Row(
          children: [
            Text('Already have an account?'),
            GestureDetector(
                child: Text('Sign in Here',
                    style: TextStyle(color: Colors.redAccent)),
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => UserLogin()));
                })
          ],
        ))
      ],
    ))));
  }

  Widget _logo() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Center(
            child: Text("Bigstack Technologies",
                softWrap: true,
                style: TextStyle(color: Colors.black26, fontSize: 16))));
  }

  // snapshot.data['name']

  Widget _buildFormField(
      {String label,
      TextEditingController controller,
      TextInputType type = TextInputType.text,
      bool secret = false}) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 35, vertical: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(label.toUpperCase(),
                style: TextStyle(
                    letterSpacing: 1.0,
                    fontSize: 15,
                    fontWeight: FontWeight.w500)),
            TextField(
              autofocus: false,
              obscureText: secret,
              keyboardType: type,
              controller: controller,
              decoration: InputDecoration(
                  // labelText: ,
                  // hintText: label,
                  labelStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                  ),
                  border: InputBorder.none),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 0.1, vertical: 0.1),
              child: Container(
                height: 2.0,
                width: MediaQuery.of(context).size.width,
                color: Colors.black12,
              ),
            )
          ],
        ));
  }

  Widget _submitButton() {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        child: ButtonTheme(
          //elevation: 4,
          //color: Colors.green,
          minWidth: double.infinity,
          child: MaterialButton(
            onPressed: () {
              userBloc.userDataStream.listen((res) {
                switch (res.status) {
                  case Status.LOADING:
                    userBloc2.userDataSink.add(res);
                    break;
                  case Status.COMPLETED:
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                    userBloc2.userDataSink.add(res);
                    break;
                  default:
                    userBloc2.userDataSink.add(res);
                }
              });
              userBloc.register(nameEditingController.text,
                  emailEditingController.text, passwordEditingController.text);
            },
            textColor: Colors.white,
            color: Theme.of(context).primaryColor,
            height: 50,
            child: Text("Sign up",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18)),
          ),
        ));
  }
}

class Loading extends StatelessWidget {
  final String loadingMessage;

  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor:
                AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
          ),
        ],
      ),
    );
  }
}
