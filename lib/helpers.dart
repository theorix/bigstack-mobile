import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:universal_html/html.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:bigstack_mobile/services/storage.dart';
import 'package:bigstack_mobile/networking/ApiProvider.dart';

final formatCurrency = NumberFormat();
final FlutterSecureStorage storage = FlutterSecureStorage();

String naira(num amount) {
  if (amount == null) return '\u{20A6}${0}';
  return '\u{20A6}${formatCurrency.format(amount)}';
}

Color getColorFromHex(String hexColor) {
  hexColor = hexColor.replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  if (hexColor.length == 8) {
    return Color(int.parse("0x$hexColor"));
  }
}

class Logger {
  final String context;
  static bool debugMode = false;
  Logger({this.context});

  log(String msg) {
    if (!debugMode) {
      print(msg);
    }
  }

  start({String message}) {
    if (!debugMode) {
      print('*********************** $context **************************');
      if (message != null) print('------------ $message -------------');
    }
  }

  end({String message}) {
    if (!debugMode) {
      print('*********************** END $context ***********************');
      if (message != null) print('------------ $message -------------');
    }
  }
}

String myDateFormat(String dateStr, [bool time = false]) {
  var d = '05/15/2020 12:05';
  var dateAndTime = dateStr.split(' ');
  var p = dateAndTime[0].split('/');
  p = p.reversed.toList();
  var dateString = p.join('-');
  if (!time) {
    if (dateAndTime.length == 1)
      return DateFormat.yMMMd().format(DateTime.parse(dateString));
    return DateFormat.yMMMd()
        .format(DateTime.parse(dateString + ' ${dateAndTime[1]}'));
  } else {
    if (dateAndTime.length == 1) {
      return DateFormat.jm().format(DateTime.parse(dateString));
    } else {
      return DateFormat.jm()
          .format(DateTime.parse(dateString + ' ${dateAndTime[1]}'));
    }
  }
  // return dateString;
}

int currentTimeInSeconds() {
  var ms = (new DateTime.now()).millisecondsSinceEpoch;
  return (ms / 1000).round();
}

Future<dynamic> fetchData(
    {@required bool cached,
    @required String cacheKey,
    @required String url}) async {
  var storageService = StorageService();
  Logger logger = Logger(context: 'Helper => fetchData');
  ApiProvider apiProvider = ApiProvider();
  JsonCodec codec = JsonCodec();
  var resp;
  if (cached) {
    logger.log('Fetching $url from Cache');
    resp = await storageService.getStoredData(cacheKey);
    if (resp == null) {
      logger.log('Cache missed, fetching $url from Network');
      resp = await apiProvider.get(url);
      logger.log('resppppp:' + resp.toString());
      storageService.storeData(cacheKey, codec.encode(resp));
      logger.log(' Network Response chached ');
    } else {
      logger.log('Cache hit');
      resp = json.decode(resp);
    }
  } else {
    logger.log(' Fetching $url from Network');
    resp = await apiProvider.get(url);
    // store news in chache
    storageService.storeData(cacheKey, codec.encode(resp));
    logger.log(' Network Response chached ');
  }
  return resp;
}
