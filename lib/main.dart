import 'package:flutter/material.dart';
import 'package:bigstack_mobile/views/splashscreen.dart';
import 'package:bigstack_mobile/views/dashboard.dart';
import 'package:bigstack_mobile/views/login.dart';
import 'package:bigstack_mobile/views/signup.dart';
import 'package:bigstack_mobile/views/payment_form.dart';
import 'package:bigstack_mobile/views/about_us.dart';
import 'package:bigstack_mobile/views/help_page.dart';
import 'package:bigstack_mobile/views/profile.dart';
import 'package:bigstack_mobile/views/finance/home.dart';
import 'package:bigstack_mobile/views/finance/income.dart';
import 'package:bigstack_mobile/views/finance/voucher.dart';
import 'package:bigstack_mobile/views/human_resource/home.dart';
import 'package:bigstack_mobile/views/hub/home.dart';
import 'package:bigstack_mobile/views/hub_page.dart';
import 'package:bigstack_mobile/views/booking_summary.dart';
import 'package:bigstack_mobile/views/admin.dart';
// import 'package:bigstack_mobile/widgets/scanQr.dart';
import 'package:bigstack_mobile/views/staff_sign_in.dart';
import 'package:bigstack_mobile/widgets/generateQr.dart';
import 'package:provider/provider.dart';

import 'enum.dart';
import 'models/menu_info.dart';

void main() {
  // runApp(MyApp());

  runApp(ChangeNotifierProvider<MenuInfo>(
      create: (context) => MenuInfo(
            MenuType.home,
          ),
      child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primaryColor: Colors.white, accentColor: Colors.green),
        home: Splashscreen(),
        routes: {
          '/dashboard': (context) => Dashboard(),
          '/login': (context) => UserLogin(),
          '/signup': (context) => UserSignup(),
          // '/hub': (context) => HubPage(),
          '/hub': (context) => Hub(),
          '/finance': (context) => Finance(),
          '/finance/income': (context) => IncomePage(),
          '/finance/voucher': (context) => VoucherPage(),
          '/hr': (context) => HR(),
          '/staff/signin': (context) => StaffSignPage(),
          '/booking/summary': (context) => BookingSummary(),
          '/admin': (context) => AdminView(),
          '/payment': (context) => PaymentScreen(),
          '/about': (context) => AboutUsPage(),
          '/profile': (context) => ProfilePage(),
          '/help': (context) => HelpPage()
        });
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
