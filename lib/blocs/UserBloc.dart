import 'dart:async';
import 'package:bigstack_mobile/networking/Response.dart';
import 'package:bigstack_mobile/repository/UserRepository.dart';
import 'package:bigstack_mobile/models/User.dart';
import 'package:bigstack_mobile/models/Hub.dart';

class UserBloc {
  StreamController<Response<User>> _userDataController;
  StreamController<Response<Map<String, dynamic>>> _bookingDataController;
  UserRepository _userRepository;

  UserBloc() {
    _userDataController = StreamController<Response<User>>.broadcast();
    _bookingDataController =
        StreamController<Response<Map<String, dynamic>>>.broadcast();
    _userRepository = UserRepository();
  }

  StreamSink<Response<User>> get userDataSink => _userDataController.sink;
  Stream<Response<User>> get userDataStream => _userDataController.stream;

  StreamSink<Response<Map<String, dynamic>>> get bookingDataSink =>
      _bookingDataController.sink;
  Stream<Response<Map<String, dynamic>>> get bookingDataStream =>
      _bookingDataController.stream;

  authenticate(String email, String password) async {
    userDataSink.add(Response<User>.loading('Authenticating user'));
    try {
      // get the data from server - i.e authenticate the user
      User resp = await _userRepository.login(email: email, password: password);
      // inspect the response
      userDataSink.add(Response<User>.completed(resp));
    } catch (e) {
      // emit the error response on failure
      userDataSink.add(Response<User>.error(e.toString()));
    }
  }

  register(String fullName, String email, String password) async {
    userDataSink.add(Response<User>.loading('Registring user'));
    try {
      // get the data from server - i.e authenticate the user
      User resp = await _userRepository.signup(
          fullName: fullName, email: email, password: password);
      // inspect the response
      userDataSink.add(Response<User>.completed(resp));
    } catch (e) {
      // emit the error response on failure
      userDataSink.add(Response<User>.error(e.toString()));
    }
  }

  bookSpace(Map<String, dynamic> hubModel) async {
    bookingDataSink
        .add(Response<Map<String, dynamic>>.loading('Booking Space'));
    try {
      var resp = await _userRepository.bookHubSpace(hubModel);
      bookingDataSink.add(Response<Map<String, dynamic>>.completed(resp));
    } catch (e) {
      // emit error
      print('Error Booking: ' + e.toString());
      bookingDataSink.add(Response<Map<String, dynamic>>.error(e.toString()));
    }
  }

  dispose() {
    _userDataController?.close();
    _bookingDataController?.close();
  }
}
