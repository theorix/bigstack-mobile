import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:universal_html/html.dart' as web;
import 'package:flutter/foundation.dart' show kIsWeb;
import 'dart:io' show Platform;

class StorageService {
  static bool storageInited = false;
  static FlutterSecureStorage _secureStorage;
  static TargetPlatform platform;
  static final StorageService _storage = StorageService._internal();

  // logger
  // static Logger logger = Logger(context: 'Storage Service');

  factory StorageService() {
    return _storage;
  }

  StorageService._internal({TargetPlatform platform}) {
    if (!storageInited) initialize();
  }

  static initialize({TargetPlatform platform}) {
    _secureStorage = FlutterSecureStorage();
    storageInited = true;
  }

  Future<Map<String, dynamic>> getUserData() async {
    Map<String, dynamic> userData;
    try {
      userData = jsonDecode(await getStoredData('user'));
    } catch (e) {
      print('UserData JSON ERROR:' + e.toString());
    }
    print('UserData' + userData.toString());
    return userData;
  }

  Future<String> getStoredData(key) async {
    // if (!storageInited) initialize();
    if (kIsWeb) {
      return web.window.localStorage[key];
    } else {
      return await _secureStorage.read(key: key);
    }
  }

  void storeData(String key, String data) async {
    // if (!storageInited) initialize();
    if (kIsWeb) {
      web.window.localStorage[key] = data;
    } else {
      await _secureStorage.write(key: key, value: data);
    }
  }

  void removeAllStoredData() async {
    if (kIsWeb) {
      web.window.localStorage.clear();
    } else {
      await _secureStorage.deleteAll();
    }
  }

  Future<String> getProfilePic() async {
    String dir = await _findLocalPath();
    return '$dir/User/p';
  }

  Future<String> _findLocalPath() async {
    final directory = Platform.isAndroid // platform == TargetPlatform.android
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }
}
