import 'dart:convert';
import 'package:flutter/material.dart';
// import 'package:permission_handler/permission_handler.dart';
// import 'package:edumobile/services/socket.dart';
import 'package:bigstack_mobile/services/storage.dart';
// import 'package:edumobile/services/firebase.dart';
import 'package:bigstack_mobile/helpers.dart';

bool appInited = false;
Logger logger = Logger(context: 'INITIALIZATION');

Future<Map<String, dynamic>> _checkSession() async {
  var userData = await StorageService().getUserData();
  if (userData != null) {
    var tokenMillisecondStr =
        await StorageService().getStoredData('tokenExpires');
    var today = currentTimeInSeconds();
    var sessionExpiryDate = tokenMillisecondStr != null
        ? int.parse(tokenMillisecondStr)
        : today + 1;
    if (sessionExpiryDate > today) {
      return {'loggedIn': true, 'user': userData};
    } else
      return {'loggedIn': false};
  } else
    return {'loggedIn': false};
}

void initApp({BuildContext context}) async {
  logger.start();
  _checkSession().then((session) {
    if (!session['loggedIn'])
      Navigator.of(context).pushReplacementNamed('/login');
    else {
      if (!appInited) {
        logger.log('INITIALIZING APP');
        StorageService.initialize();
        appInited = true;
        Navigator.of(context).pushReplacementNamed('/dashboard');
        logger.log('DONE');
      } else {
        logger.log('APP ALREADY INITIALIZED');
      }
    }
    logger.end();
  });
}
